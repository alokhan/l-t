// Copyright 2013 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package junit;

import junit.framework.TestCase;
import pass.Octal;

public class OctalTest extends TestCase {
	
	public Octal octal;

    protected void setUp() throws Exception {
        super.setUp();
        octal = new Octal();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testOctal() {
        this.assertEquals(octal.TenInOctal(), 10);
    }

}

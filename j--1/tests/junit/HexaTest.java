// Copyright 2013 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package junit;

import junit.framework.TestCase;
import pass.Hexa;

public class HexaTest extends TestCase {
	
	public Hexa hexa;

    protected void setUp() throws Exception {
        super.setUp();
        hexa = new Hexa();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testHexa() {
        this.assertEquals(hexa.TenInHexa(), 10);
    }

}

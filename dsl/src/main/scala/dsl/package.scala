
import edu.uci.ics.jung.graph._
import edu.uci.ics.jung.algorithms.layout._
import edu.uci.ics.jung.visualization._
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Paint;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Color;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;

package object graphDsl {

  //EDGE
  abstract class EdgeBuilder(val vertexS : Vertex, val vertexA: Vertex, val label: Label,val stroke: Boolean, val width:Int, val color: Paint,val oriented: Boolean){
    def this(vertexS : Vertex, vertexA: Vertex, oriented: Boolean){
      this(vertexS, vertexA, new Label("generic name"),false, 10, Color.black,oriented)
    }
    def this(vertexS : Vertex, vertexA: Vertex, label: Label, oriented: Boolean){
      this(vertexS, vertexA,label,false, 10, Color.black,oriented)
    }
    def this(vertexS : Vertex, vertexA: Vertex, stroke: Boolean, width: Int, oriented: Boolean){
      this(vertexS, vertexA, new Label("generic name"), stroke, width, Color.black,oriented)
    }
    def this(vertexS : Vertex, vertexA: Vertex, label: Label, stroke: Boolean, width: Int, oriented: Boolean){
      this(vertexS, vertexA,label, stroke, width, Color.black,oriented)
    }
    def this(vertexS : Vertex, vertexA: Vertex, stroke: Boolean, width: Int, color: Color, oriented: Boolean){
      this(vertexS, vertexA, new Label("generic name"),stroke, width, color,oriented)
    }
  }

  class EdgeBuilder1(vertexS : Vertex, vertexA: Vertex,oriented: Boolean) extends EdgeBuilder(vertexS, vertexA, oriented) {
    def labeled(label: Label) = {
      new EdgeBuilder2(vertexS,vertexA,label,oriented)
    }
    def normal(width: Int) = new EdgeBuilder3(vertexS,vertexA,false,width,oriented)
    def stroked(width: Int) = new EdgeBuilder3(vertexS,vertexA,true,width,oriented)
  }

  class EdgeBuilder2(vertexS : Vertex, vertexA: Vertex,label: Label,oriented: Boolean) extends EdgeBuilder(vertexS, vertexA,label,oriented){
    def normal(width: Int) = new EdgeBuilder4(vertexS,vertexA,label,false,width,oriented)
    def stroked(width: Int) = new EdgeBuilder4(vertexS,vertexA,label,true,width,oriented)
  }

  class EdgeBuilder3(vertexS : Vertex, vertexA: Vertex, stroke: Boolean, width:Int,oriented: Boolean) extends EdgeBuilder(vertexS, vertexA, stroke, width,oriented){
    def colored(color: Color) = {
      new EdgeBuilder5(vertexS,vertexA,stroke,width,color,oriented)
    }
  }

  class EdgeBuilder4(vertexS : Vertex, vertexA: Vertex, label: Label, stroke: Boolean, width:Int,oriented: Boolean) extends EdgeBuilder(vertexS, vertexA, label, stroke, width,oriented){
    def colored(color: Color) = new EdgeBuilder6(vertexS,vertexA,label,stroke,width, color,oriented)
  }

  class EdgeBuilder5(vertexS : Vertex, vertexA: Vertex, stroke: Boolean, width:Int, color: Color,oriented: Boolean) extends EdgeBuilder(vertexS, vertexA, stroke, width, color,oriented){

  }

  class EdgeBuilder6(vertexS : Vertex, vertexA: Vertex, label: Label, stroke: Boolean, width:Int, color:Color,oriented: Boolean) extends EdgeBuilder(vertexS, vertexA, label, stroke, width, color,oriented){

  }

  //SHAPE
  class ShapeColored (var vertex: Vertex, var form: Shape, var color : Paint)

  class MyShape (var vertex: Vertex, var form: Shape){
    def apply(vertex: Vertex, shape: Shape) = new MyShape(vertex, shape)
    def colored(color: Paint) = {
      new ShapeColored (this.vertex, this.form, color)
    }
  }

  class Vertex (var value: String){
    // oriented edge
    def -->(vertexA: Vertex) = {
      new EdgeBuilder1(this,vertexA,true)
    }
    // non oriented edge
    def --(vertexA: Vertex) = {
      new EdgeBuilder1(this,vertexA,false)
    }
    // shape of the vertex
    def as(shape: Shape) = {
      new MyShape(this,shape)
    }
  }

  class Label (var value: String){

  }

  class StrokeWidth (var value: Int){

  }

  // Layout type for the graph layout
  abstract class LayoutType {}

  class CircleLayoutType extends LayoutType{ }
  object CircleLayoutType {
    def apply() = new CircleLayoutType()
  }

  class KKLayoutType extends LayoutType{ }
  object KKLayoutType {
    def apply() = new KKLayoutType()
  }

  class FRLayoutType extends LayoutType{ }
  object FRLayoutType {
    def apply() = new FRLayoutType()
  }

  class FRLayout2Type extends LayoutType{ }
  object FRLayout2Type {
    def apply() = new FRLayout2Type()
  }

  class ISOMLayoutType extends LayoutType{ }
  object ISOMLayoutType {
    def apply() = new  ISOMLayoutType()
  }

  class  StaticLayoutType extends LayoutType {}
  object StaticLayoutType {
    def apply() = new  StaticLayoutType()
  }


  // Shape type for the vertex
  object Rectangle {
    def apply(x: Int, y: Int) = new Rectangle(x,y)
  }

  object Circle {
    def apply(w: Int) = new Ellipse2D.Double(0,0, w, w)
  }

  object Square {
    def apply(x: Int) = new Rectangle(x,x)
  }

  implicit class GraphOps(graph : dsl.Graph){

    def layoutType (layout: LayoutType) = {
      layout match {
        case _: CircleLayoutType => graph.setLayout(new CircleLayout[String,String](graph.graph))
        case _: KKLayoutType => graph.setLayout(new KKLayout[String,String](graph.graph))
        case _: FRLayoutType => graph.setLayout(new FRLayout[String,String](graph.graph))
        case _: FRLayout2Type => graph.setLayout(new FRLayout2[String,String](graph.graph))
        case _: ISOMLayoutType => graph.setLayout(new ISOMLayout[String,String](graph.graph))
        case _: StaticLayoutType => graph.setLayout(new StaticLayout[String,String](graph.graph))
        case _ => false
      }

    }

    def += (vertex: Vertex) : Vertex = { graph.addVertex(vertex); return vertex;}

    def -= (vertex: Vertex) : Vertex = { graph.removeVertex(vertex); return vertex;}

    def += (edge : EdgeBuilder)  = {
      edge match {
        case edgeCast: EdgeBuilder1 => graph.addEdge(edgeCast.vertexS, edgeCast.vertexA,edgeCast.oriented)
        case _: EdgeBuilder2 => graph.addEdge(edge.label,edge.vertexS, edge.vertexA,edge.oriented)
        case _: EdgeBuilder3 => {val name = graph.addEdge(edge.vertexS, edge.vertexA,edge.oriented); graph.SetEdgeType(name, edge.stroke); graph.setEdgeSize(name, edge.width)}
        case _: EdgeBuilder4 => {graph.addEdge(edge.label,edge.vertexS, edge.vertexA,edge.oriented); graph.SetEdgeType(edge.label, edge.stroke); graph.setEdgeSize(edge.label, edge.width)}
        case _: EdgeBuilder5 => {val name = graph.addEdge(edge.vertexS, edge.vertexA,edge.oriented); graph.SetEdgeType(name, edge.stroke); graph.setEdgeSize(name, edge.width); graph.setEdgeColor(name, edge.color);}
        case _: EdgeBuilder6 => {graph.addEdge(edge.label,edge.vertexS, edge.vertexA,edge.oriented); graph.SetEdgeType(edge.label, edge.stroke); graph.setEdgeSize(edge.label, edge.width); graph.setEdgeColor(edge.label, edge.color);}
      }
    }

    def -= (edge: EdgeBuilder)  = {
      edge match {
        case _: EdgeBuilder => graph.removeEdge(edge.label,edge.vertexS, edge.vertexA)
      }
    }

    def += (shape: MyShape) : MyShape = { graph.addVertex(shape.vertex); graph.setShape(shape.vertex, shape.form); return shape;}

    def += (shape: ShapeColored) : ShapeColored = { graph.addVertex(shape.vertex); graph.setShape(shape.vertex, shape.form); graph.setColor(shape.vertex, shape.color); return shape;}

  }

  object VertexImplicit{
    implicit def String2Vertex(value : String) : Vertex = {
      new Vertex(value)
    }
  }

  object LabelImplicit{
    implicit def String2Label(value : String) : Label = {
      new Label(value)
    }
  }

  /*object ShapeImplicit{
  implicit def String2Shape(value : String) : Shape = {
  new Shape(value)
}
}*/


}

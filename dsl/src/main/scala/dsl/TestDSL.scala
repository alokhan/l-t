package dsl
import graphDsl._
import graphDsl.VertexImplicit.String2Vertex
import graphDsl.LabelImplicit.String2Label
import java.awt.Color;

object TestDSL extends App {

  val graph = new Graph

  // add vertex
  graph += "vertex1" as Rectangle(20,40) colored Color.yellow
  graph += "vertex2" as Rectangle(20,40)  colored Color.cyan
  graph +=  "vertex3" as Rectangle(20,40)
  graph +=  "vertex4" as Rectangle(20,40)

  // add non oriented edge
  graph +=  "vertex1" --> "vertex2" labeled "edge1"

  // add oriented edges
  graph += "vertex2" --> "vertex1"
  graph += "vertex2" --> "vertex3" labeled "edge3"
  graph += "vertex3" --> "vertex2" normal 10 colored Color.green

  // remove a vertex
  graph -= "vertex4"
  // remove a edge
  graph -= "vertex2" --> "vertex3"

  // set graph layout
  graph layoutType FRLayoutType()

  // build graph
  graph build

  graph.graph.addVertex("Direct access")

  // display graph
  graph show
}

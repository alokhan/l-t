package dsl
import edu.uci.ics.jung.graph._
import edu.uci.ics.jung.algorithms.layout._
import edu.uci.ics.jung.visualization._
import javax.swing._
import org.apache.commons.collections15.Transformer
import java.awt._;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import graphDsl.Vertex
import graphDsl.Label
import graphDsl.EdgeBuilder2
import scala.collection.mutable._
import java.awt.Paint
import edu.uci.ics.jung.visualization.control._
import org.apache.commons.collections15.Factory


class Graph() {
	var graph : AbstractGraph[String,String] = new UndirectedSparseGraph[String,String]

	var shapeMap : HashMap[String,Shape] = new HashMap[String,Shape]()
	var colorMap : HashMap[String,Paint] = new HashMap[String,Paint]()
	var edgeFormMap : HashMap[String, Boolean] = new HashMap[String, Boolean]()
	var edgeSizeMap : HashMap[String, Int] = new HashMap[String, Int]()
	var edgeColorMap : HashMap[String, Paint] = new HashMap[String, Paint]()
	var layout : Layout[String,String] = new CircleLayout[String,String](graph)
	var vertexList: MutableList[String] = new MutableList[String]()
	var edgeMap: MutableList[Tuple3[String,String,String]] = new MutableList[Tuple3[String,String,String]]()

	var vertexIndex = 0
	var edgeIndex = 0
	var orientedGraph = false


	def addVertex(vertex: Vertex) = vertexList += (vertex.value)
	def removeVertex(vertex: Vertex) = vertexList = vertexList.filterNot(elm => elm == vertex.value)
	def addEdge(label: Label, vertex1: Vertex, vertex2: Vertex, oriented: Boolean) : Label  =
		{
			// if the edge is oriented change to type of graph
			if (oriented){
				orientedGraph = true
			}
			graph.addEdge(label.value,vertex1.value,vertex2.value)
			edgeMap += new Tuple3(label.value,vertex1.value,vertex2.value)
			label
		}
		def addEdge( vertex1: Vertex, vertex2: Vertex, oriented: Boolean) : Label  = {
			val old = edgeIndex
			edgeIndex = edgeIndex +1;
			this.addEdge(new Label(old.toString()),vertex1,vertex2,oriented)
		}

		def removeEdge(label: Label,vertex1: Vertex, vertex2: Vertex) = edgeMap = edgeMap.filterNot(elm => elm._2 == vertex1.value && elm._3 == vertex2.value)

		def setShape(vertex: Vertex, shape: Shape): Boolean = {shapeMap += (vertex.value -> shape); true}
		def setColor(vertex: Vertex, color: Paint): Boolean = {colorMap += (vertex.value -> color); true}
		def SetEdgeType(label: Label, stroke: Boolean): Boolean = {edgeFormMap += (label.value -> stroke); true}
		def setEdgeColor(label: Label, color: Paint): Boolean = { edgeColorMap += (label.value -> color); true}
		def setEdgeSize(label: Label, size: Int): Boolean = {edgeSizeMap += (label.value-> size); true}
		def setLayout(layout: Layout[String,String]) = { this.layout = layout}

		def build = {
			// if it is a oriented graph change the type to oriented
			if (orientedGraph){
				graph = new DirectedSparseGraph[String,String]()
			}

			// add vertex and edge to the graph
			vertexList.map(x => graph.addVertex(x))
			edgeMap.map(x => graph.addEdge(x._1, x._2, x._3))
		}

		def show = {

			layout.setGraph(graph)
			val vv = new VisualizationViewer[String, String](layout)
			val transformer = new Transformer[String, String] {
				override def transform(label: String): String = label
			}

			val shapeTransformer = new Transformer[String,Shape] {
				override def transform(vertex:String) : Shape = {
					if (shapeMap.contains(vertex)) {
						shapeMap(vertex)
					}
					else {
						new Rectangle(0,0, 20, 20)
					}
				}
			}

			val colorTransformer = new Transformer[String, Paint] {
				override def transform(vertex:String) : Paint = {
					if (colorMap.contains(vertex)) {
						colorMap(vertex)
						/*val a = Color.green
						val method = a.getClass.getField(colorMap(vertex))
						method.get(a).asInstanceOf[Paint]
						*/
					}
					else{
						Color.red
					}
				}
			}

			val edgeStrokeTransformer = new Transformer[String,Stroke] {
				val dash = new Array[Float](1)
				override def transform(edge: String) : Stroke = {
					if (edgeFormMap.contains(edge)) {
						if(edgeFormMap(edge)){dash(0) = 10.0f; return new BasicStroke(edgeSizeMap(edge), BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f)}
						else{ return new BasicStroke(edgeSizeMap(edge))}
					}else{
						return new BasicStroke(1);
					}

				}
			}

			val edgePaintTransformer = new Transformer[String,Paint] {
				override def transform(edge:String) : Paint = {
					if (edgeColorMap.contains(edge)) {
						return edgeColorMap(edge)
					}
					else{
						Color.black
					}
				}
			}


			//CADEAU http://jung.sourceforge.net/doc/api/edu/uci/ics/jung/visualization/RenderContext.html
			vv.getRenderContext.setVertexLabelTransformer(transformer)
			vv.getRenderContext.setEdgeLabelTransformer(transformer)
			vv.getRenderContext.setEdgeDrawPaintTransformer(edgePaintTransformer)
			vv.getRenderContext.setEdgeStrokeTransformer(edgeStrokeTransformer)
			vv.getRenderContext.setVertexShapeTransformer(shapeTransformer)
			vv.getRenderContext.setVertexFillPaintTransformer(colorTransformer)
			//vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
			//vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller());

			// enable GUI editing of the graph
			var gm = new EditingModalGraphMouse[String,String](vv.getRenderContext(),vertexFactory,edgeFactory)
			gm.setMode(ModalGraphMouse.Mode.EDITING);
			vv.setGraphMouse(gm);
			var menuBar = new JMenuBar();
			var modeMenu = gm.getModeMenu(); // Obtain mode menu from the mouse
			modeMenu.setText("Mouse Mode");
			modeMenu.setIcon(null);
			modeMenu.setPreferredSize(new Dimension(80,20));
			menuBar.add(modeMenu);

			val frame = new JFrame
			frame.getContentPane.add(vv)
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
			frame.setJMenuBar(menuBar);
			frame.pack()
			frame.setVisible(true)
		}

		// factory for vertex and edge when creating with GUI
		val vertexFactory = new Factory[String] {
			override def create(): String = 	{
				val old = vertexIndex
				vertexIndex += 1;
				old.toString()
			}
		}

		val edgeFactory = new Factory[String] {
			override def create(): String = 	{
				val old = edgeIndex
				edgeIndex += 1;
				old.toString()
			}
		}
	}

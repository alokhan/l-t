package grammar;

import grammar.Parser.Tuple;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Stack;

public class Parser {

	private Stack<String> stack;

	private HashMap<Tuple, Integer> hashMap;

	private String[][] rules;

	// modify this
	public boolean parse(String[] input) {
		rules = new String[][] { 
				{ "useless" }, 
				{ "T", "B" },
				{ "+", "T", "B" }, 
				{ "-", "T", "B" }, 
				{ "empty" }, 
				{ "F", "C" },
				{ "*", "F", "C" },
				{ "/", "F", "C" }, 
				{ "empty" },
				{ "(", "E", ")" }, 
				{ "id" } };

		// construct rules table
		// rules[1] = "TB";
		// rules[2] = "+TB";
		// rules[3] = "-TB";
		// rules[4] = "";
		// rules[5] = "FC";
		// rules[6] = "*FC";
		// rules[7] = "/FC";
		// rules[8] = "";
		// rules[9] = "(E)";
		// rules[10] = "id";

		// construct parsing table
		stack = new Stack<String>();
		hashMap = new HashMap<Tuple, Integer>();
		hashMap.put(new Tuple("E", "("), 1);
		hashMap.put(new Tuple("E", "id"), 1);

		hashMap.put(new Tuple("B", "+"), 2);
		hashMap.put(new Tuple("B", "-"), 3);
		hashMap.put(new Tuple("B", ")"), 4);
		hashMap.put(new Tuple("B", "#"), 4);

		hashMap.put(new Tuple("T", "("), 5);
		hashMap.put(new Tuple("T", "id"), 5);

		hashMap.put(new Tuple("C", "+"), 8);
		hashMap.put(new Tuple("C", "-"), 8);
		hashMap.put(new Tuple("C", "*"), 6);
		hashMap.put(new Tuple("C", "/"), 7);
		hashMap.put(new Tuple("C", ")"), 8);
		hashMap.put(new Tuple("C", "#"), 8);

		hashMap.put(new Tuple("F", "("), 9);
		hashMap.put(new Tuple("F", "id"), 10);

		// stack
		stack.push("#");
		stack.push("E");
		// Parse the string expression
		String[] tab = new String[input.length + 1];
		for (int i = 0; i < input.length; i++) {
			tab[i] = input[i];
		}
		tab[tab.length - 1] = "#";

		int pointer = 0;
		String elemPointer;
		while (pointer < tab.length) {
			// get the element from the string array
			elemPointer = tab[pointer];
			if (CheckInt(elemPointer)) {
				elemPointer = "id";
			}

			if (elemPointer.equals(stack.peek())) {
				pointer++;
				stack.pop();
			} else {
				// getting the rule from parsing table if no rule ==> ERROR
				Integer ruleNbr = hashMap.get(new Tuple(stack
						.peek(), elemPointer));
				if (ruleNbr == null) {
					return false;
				}

				stack.pop();
				// parsing the rules and push to stack
				if (rules[ruleNbr][0] != "empty") {
					for (int i = rules[ruleNbr].length - 1; i >= 0; i--) {
						stack.push(rules[ruleNbr][i]);
						
					}
				}
			}

		}

		return true;
	}

	private boolean CheckInt(String p) {
		for (char i : p.toCharArray()) {
			if (i < '0' || i > '9') {
				return false;
			}
		}
		return true;
	}

	public class Tuple {
		public final String x;
		public final String y;

		public Tuple(String x, String y) {
			this.x = x;
			this.y = y;
		}
		
		@Override
	    public int hashCode(){
			return this.x.hashCode() + this.y.hashCode();
		}

		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			Tuple other = (Tuple) obj;
			return other != null && this.x.equals(other.x)
					&& this.y.equals(other.y);

		}
	}

	public static void main(String[] main) {
		Generator gen = new Generator(2);
		Parser parser = new Parser();
		String[] emptyTab = new String[0];
		String[] bidonTab = {"(","5","+","+","2",")", "*", "2"}; 
		System.out.println(parser.parse(gen.generate(10)));
		System.out.println(parser.parse(gen.generate(9)));
		System.out.println(parser.parse(emptyTab));
		System.out.println(parser.parse(bidonTab));

	}

}

package lingi2132;

import static jminusminus.CLConstants.ALOAD_0;
import static jminusminus.CLConstants.BIPUSH;
import static jminusminus.CLConstants.ICONST_0;
import static jminusminus.CLConstants.ICONST_1;
import static jminusminus.CLConstants.ICONST_2;
import static jminusminus.CLConstants.ICONST_3;
import static jminusminus.CLConstants.ICONST_4;
import static jminusminus.CLConstants.ICONST_5;
import static jminusminus.CLConstants.IF_ICMPGE;
import static jminusminus.CLConstants.ILOAD_0;
import static jminusminus.CLConstants.INVOKESPECIAL;
import static jminusminus.CLConstants.IRETURN;
import static jminusminus.CLConstants.RETURN;
import static jminusminus.CLConstants.SIPUSH;
import static jminusminus.CLConstants.ISUB;
import static jminusminus.CLConstants.INVOKESTATIC;
import static jminusminus.CLConstants.IADD;

import java.util.ArrayList;

import jminusminus.CLEmitter;

public class Generator extends FibonacciGenerator {

	private CLEmitter clEmitter;

	public Generator(String outputDir) {
		super(outputDir);
		// TODO Auto-generated constructor stub
		clEmitter = new CLEmitter(true);
	}

	public void generateClass() {
		// set the output dir
		clEmitter.destinationDir(this.outputDir);

		ArrayList<String> accessFlagsClass = new ArrayList<String>();
		accessFlagsClass.add("public");
		accessFlagsClass.add("super");
		
		String fullyQualifiedClassName = "lingi2132/ClassToGenerate";
		String fullyQualifiedSuperClassName = "java/lang/Object";

		// first add class
		clEmitter.addClass(accessFlagsClass, fullyQualifiedClassName,
				fullyQualifiedSuperClassName, null, false);
		
		// add empty constructor
		ArrayList<String> mods = new ArrayList<String>();
		mods.add("public");
		clEmitter.addMethod(mods, "<init>", "()V", null, false);
		clEmitter.addNoArgInstruction(ALOAD_0);
		clEmitter.addMemberAccessInstruction(INVOKESPECIAL,
				fullyQualifiedSuperClassName, "<init>", "()V");
		clEmitter.addNoArgInstruction(RETURN);

		// add the method fibonacci
		ArrayList<String> methodAccessFlags = new ArrayList<String>();
		methodAccessFlags.add("public");
		methodAccessFlags.add("static");
		String methodName = "fibonacci";
		clEmitter.addMethod(methodAccessFlags, methodName, "(I)I", null, false);

		// finaly add the method body
		String elseLabel = clEmitter.createLabel();

		// push the two int 2 and n on top of the stack
		// load parameter
		clEmitter.addNoArgInstruction(ILOAD_0);
		// load 2
		clEmitter.addNoArgInstruction(ICONST_2);

		// generate the condition n < 2
		// branch on else label if yes
		clEmitter.addBranchInstruction(IF_ICMPGE, elseLabel);

		// if condition is false then return n
		clEmitter.addNoArgInstruction(ILOAD_0);
		clEmitter.addNoArgInstruction(IRETURN);

		clEmitter.addLabel(elseLabel);
		clEmitter.addNoArgInstruction(ILOAD_0);
		clEmitter.addNoArgInstruction(ICONST_1);
		clEmitter.addNoArgInstruction(ISUB);
		clEmitter.addMemberAccessInstruction(INVOKESTATIC,
				fullyQualifiedClassName, "fibonacci", "(I)I");

		clEmitter.addNoArgInstruction(ILOAD_0);
		clEmitter.addNoArgInstruction(ICONST_2);
		clEmitter.addNoArgInstruction(ISUB);
		clEmitter.addMemberAccessInstruction(INVOKESTATIC,
				fullyQualifiedClassName, "fibonacci", "(I)I");

		clEmitter.addNoArgInstruction(IADD);
		clEmitter.addNoArgInstruction(IRETURN);
		
		clEmitter.write();

	}

	// helper method to push int on top of the stack
	public void writeInt(CLEmitter output, int i) {
		switch (i) {
		case 0:
			output.addNoArgInstruction(ICONST_0);
			break;
		case 1:
			output.addNoArgInstruction(ICONST_1);
			break;
		case 2:
			output.addNoArgInstruction(ICONST_2);
			break;
		case 3:
			output.addNoArgInstruction(ICONST_3);
			break;
		case 4:
			output.addNoArgInstruction(ICONST_4);
			break;
		case 5:
			output.addNoArgInstruction(ICONST_5);
			break;
		default:
			if (i >= 6 && i <= 127) {
				output.addOneArgInstruction(BIPUSH, i);
			} else if (i >= 128 && i <= 32767) {
				output.addOneArgInstruction(SIPUSH, i);
			} else {
				output.addLDCInstruction(i);
			}
		}

	}

}

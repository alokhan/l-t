package pass;

public class DoWhile {
	public int DoWhileEqualsTo(int a){
		int i = 0;
		do 
		{ 
			++i; 
		} 
		while 
			(a > i);
		return i;
	}
}
// Copyright 2013 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package junit;

import junit.framework.TestCase;
import pass.DoWhile;

public class DoWhileTest extends TestCase {
	
	public DoWhile doW;

    protected void setUp() throws Exception {
        super.setUp();
        doW = new DoWhile();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testDoWhile() {
        this.assertEquals(10,doW.DoWhileEqualsTo(10));
        this.assertEquals(1,doW.DoWhileEqualsTo(0));	
        this.assertEquals(1,doW.DoWhileEqualsTo(1));
    }

}

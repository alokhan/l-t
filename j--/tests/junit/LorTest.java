// Copyright 2013 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package junit;

import junit.framework.TestCase;
import pass.Lor;

public class LorTest extends TestCase {
	
	public Lor lor;

    protected void setUp() throws Exception {
        super.setUp();
        lor = new Lor();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testLor() {
        this.assertEquals(lor.GetOrResult(false, false), false);
        this.assertEquals(lor.GetOrResult(true, false), true);
        this.assertEquals(lor.GetOrResult(false, true), true);
        this.assertEquals(lor.GetOrResult(true, true), true);
        
    }

}
